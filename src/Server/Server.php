<?php
    namespace Server;

    use \Swoole\Http\Server as SwooleHttpServer;
    use App\AppMain as App;

final class Server
{
    public function start()
    {
        $http = new SwooleHttpServer("0.0.0.0", 9501);
    
        $http->on('start', function ($server) {
            echo "Swoole http server is started at http://127.0.0.1:9501\n";
        });
    
        $http->on('request', function (
            \Swoole\Http\Request $request,
            \Swoole\Http\Response $response
        ) {
            $app = new App();
            $resultRaw = $app->handleRequest($request);
            $resultJson = json_encode($resultRaw);
            $response->header('Content-Type', 'application/json');
            $response->end($resultJson);
        });
    
        $http->start();
    }
}