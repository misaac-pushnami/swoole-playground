<?php
    namespace App\Http;

final class Api
{
    private $apiKey = '5e1f916d3d589fb32646a7f6fadd206cf32ecd2dd0ca8';
    private $apiBaseUrl = 'https://api.linkpreview.net/?';

    public function get(string $url) : array
    {
        $externalRequestUrl = $this->apiBaseUrl
            . 'key=' . $this->apiKey
            . '&q=' . $url;
        $curlOptions = [
            CURLOPT_URL => $externalRequestUrl,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_RETURNTRANSFER => true,
        ];
        $response = $this->executeCurl($curlOptions);
        return $response;
    }

    private function executeCurl(array $curlOptions) : array
    {
        $curl = curl_init();
        curl_setopt_array($curl, $curlOptions);
        $responseRaw = curl_exec($curl);
        if (curl_error($curl)) {
            throw new \Exception(curl_error($curl));
        }
        $info = curl_getinfo($curl);
        curl_close($curl);
        $responseParsed = json_decode($responseRaw, true);
        unset($info['url']);
        $responseParsed['curl_info'] = $info;
        return $responseParsed;
    }
}