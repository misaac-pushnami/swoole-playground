<?php
    namespace App;

    use App\Http\Api;

final class AppMain
{
    public function handleRequest(\Swoole\Http\Request $request) : array
    {
        $api = new Api();
        $url = $request->get['url'];
        if (is_null($url)) {
            $url = 'https://google.com';
        }
        $response = $api->get($url);
        return $response;
    }
}