#! /bin/bash
function createLogFolder {
    mkdir log
}

function getConfig {
    # Execute config script
    . $(pwd)/scripts/config.sh
}

function installComposer {
    docker run --rm \
        -v $(pwd):/app \
        -w /app \
        ${IMAGE}-dev \
        "composer install"
}

function installWatcher {
    sudo apt-get install inotify-hookable
}

getConfig
installComposer
installWatcher
createLogFolder