#! /bin/bash

function cleanup {
    docker stop ${PROJECT}
}

function getConfig {
    # Execute config script
    . $(pwd)/scripts/config.sh
}

function log {
    docker logs -f ${PROJECT} \
        &> ${LOG_FILE} &
}

function start {
    docker run --rm -d \
        --name ${PROJECT} \
        -p 9501:9501 \
        -v $(pwd):/app \
        -w /app \
        ${IMAGE} \
        "php entrypoint.php"
}

function watch {
    inotify-hookable \
        --watch-directories ./src \
        --on-modify-command './scripts/reload.sh'
}

getConfig
start
log
watch
trap cleanup EXIT