#! /bin/bash

function getConfig {
    # Execute config script
    . $(pwd)/scripts/config.sh
}

function log {
    docker logs -f ${PROJECT} \
        &> ${LOG_FILE} &
}

function restartContainer {
    docker restart ${PROJECT}
}

getConfig
restartContainer
log