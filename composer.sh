docker run --rm \
    -v $(pwd):/app \
    -w /app \
    phpswoole/swoole:4.4.5-php7.2-dev \
    "composer $1 $2"

sudo chown -R ${USER} vendor